#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__


#include "cocos2d.h"
#include "TGestureRecognizer.h"


USING_NS_CC;


class HelloWorld : public Layer, public TGestureHandler
{
public:
    static Scene* createScene();
    CREATE_FUNC(HelloWorld);
    
public:
    HelloWorld();
    virtual ~HelloWorld();
    virtual bool init();
    
public:
    // TGestureHandler's stuff
    virtual bool onGestureTap(TGestureTap* gesture);
    virtual bool onGestureLongPress(TGestureLongPress* gesture);
    virtual bool onGesturePan(TGesturePan* gesture);
    virtual bool onGesturePinch(TGesturePinch* gesture);
    virtual bool onGestureRotation(TGestureRotation* gesture);
    virtual bool onGestureSwipe(TGestureSwipe* gesture);
    
public:
    // Touch handler (pass touches to the Gesture recognizer to process)
    virtual bool TouchBegan(Touch* touch, Event* event);
    virtual void TouchMoved(Touch* touch, Event* event);
    virtual void TouchEnded(Touch* touch, Event* event);
    virtual void TouchCancelled(Touch* touch, Event* event);
    
private:
    void initGestureRecognizer();
    void setInfoLabel(const std::string& text);
    
protected:
    TGestureRecognizer* _gestureRecognizer;
    
    Sprite* _sprite;
    LabelTTF* _label;
};


#endif // __HELLOWORLD_SCENE_H__
